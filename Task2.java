package DataMining;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Scanner;

import Semestrovka.IntroSort;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

public class Task2 {
    public static void main(String[] args) throws IOException {
        Workbook wb = new HSSFWorkbook();
        Sheet sheet = wb.createSheet("Answer");
        FileOutputStream fos = new FileOutputStream("answer.xls");
        BufferedReader br = new BufferedReader(
                new InputStreamReader(new FileInputStream("transactions.csv")));
        HashMap<String, Integer> products = new HashMap<>();
        String prod = br.readLine();
         prod = br.readLine();
        while (prod != null) {
            prod = prod.split(";")[0];
            if (products.containsKey(prod)) {
                int y = products.get(prod);
                products.put(prod, y + 1);
            } else {
                products.put(prod, 1);
            }
            prod = br.readLine();
        }
        workExcel(sheet, products);
        wb.write(fos);
        fos.close();
    }


    public static void workExcel(Sheet sheet, HashMap<String, Integer> ans) {
        sheet.createRow(0).createCell(0).setCellValue("PodNum");
        sheet.getRow(0).createCell(1).setCellValue("Quantity");
        int i = 1;
        for (String s : ans.keySet()) {
            sheet.createRow(i).createCell(0).setCellValue(s);
            sheet.getRow(i).createCell(1).setCellValue(ans.get(s));
            i++;
        }
    }

//TODO: Не сделал только график.
}
